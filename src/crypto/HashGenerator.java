package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import util.DataHelper;

public class HashGenerator {
	
	private String hashingAlgo;
	private MessageDigest messageDigest;

	public HashGenerator(String hashingAlgo) throws NoSuchAlgorithmException {
		super();
		this.hashingAlgo = hashingAlgo;
		messageDigest = MessageDigest.getInstance(hashingAlgo);
	}
	
	public String getHash(String msg) {
		return DataHelper.byteArrayToHexaStr(messageDigest.digest(DataHelper.hexaStrToByteArray(msg)));
	
	}

}
