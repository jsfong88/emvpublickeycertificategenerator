package crypto;


import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * !!!WARNING!!! For testing Only. Not For product
 *
 * Rsa implementation
 * http://pajhome.org.uk/crypt/rsa/implementation.html
 */
public class Rsa implements SigningAlgo {

    private BigInteger p, q, n, d, e;

    /*
        Constructor
     */
    //For input key modulus, exponents.
    public Rsa(BigInteger n, BigInteger d, BigInteger e) {
        this.n = n;
        this.d = d;
        this.e = e;
    }

    //For generating key using prime number
    public Rsa(BigInteger p, BigInteger q, BigInteger e, int bitlen) {
        this.p = p;
        this.q = q;
        this.e = e;

        n = p.multiply(q);
        BigInteger m = (p.subtract(BigInteger.ONE))
                .multiply(q.subtract(BigInteger.ONE));
        while(m.gcd(e).intValue() > 1) e = e.add(new BigInteger("2"));
        d = e.modInverse(m);

    }    
    
    public Rsa(int keySize) {
    	generateKeyPair(keySize);
    }

    public BigInteger encrypt(BigInteger message)
    {
        return message.modPow(e, n);
    }

    public BigInteger decrypt(BigInteger message)
    {
        return message.modPow(d, n);
    }
    
    public void generateKeyPair(int keySize) {
    	
    	try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(keySize);
			KeyPair kp = keyGen.genKeyPair();
			Key publicKey = kp.getPublic();
			Key privateKey = kp.getPrivate();
			
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pub = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
			RSAPrivateKeySpec priv = (RSAPrivateKeySpec) keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
			
			n = priv.getModulus();
			d = priv.getPrivateExponent();
			e = pub.getPublicExponent(); 
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    
    //Getter and Setter
	public BigInteger getP() {
		return p;
	}

	public void setP(BigInteger p) {
		this.p = p;
	}

	public BigInteger getQ() {
		return q;
	}

	public void setQ(BigInteger q) {
		this.q = q;
	}

	public BigInteger getN() {
		return n;
	}

	public void setN(BigInteger n) {
		this.n = n;
	}

	public BigInteger getD() {
		return d;
	}

	public void setD(BigInteger d) {
		this.d = d;
	}

	public BigInteger getE() {
		return e;
	}

	public void setE(BigInteger e) {
		this.e = e;
	}
    
    
}
