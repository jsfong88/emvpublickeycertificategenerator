package crypto;

import java.math.BigInteger;

public interface SigningAlgo {

	public BigInteger encrypt(BigInteger message);
    public BigInteger decrypt(BigInteger message);
}
