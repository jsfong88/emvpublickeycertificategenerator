package crypto;

import java.math.BigInteger;

import crypto.SigningAlgo;
import util.DataHelper;

public class SignedCertificateGenerator {
	
	private SigningAlgo signingAlgo;

	public SignedCertificateGenerator(SigningAlgo signingAlgo) {
		super();
		this.signingAlgo = signingAlgo;
	}
	
	//Using Public key to design
	public String recoverCertificateSignature(String msg) {
		BigInteger outputMsg = this.signingAlgo.encrypt(DataHelper.stringtoBigInteger(msg));        
        return  DataHelper.bigIntegertoString(outputMsg);
        
	}
	
	//Using Priv key to Sign
	public String signCertificate(String msg) {
		BigInteger outputMsg = this.signingAlgo.decrypt(DataHelper.stringtoBigInteger(msg));        
        return  DataHelper.bigIntegertoString(outputMsg);
	}

}
