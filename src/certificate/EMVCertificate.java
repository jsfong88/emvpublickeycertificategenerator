package certificate;

public class EMVCertificate {

	protected String certificateFormat;	
	protected String issuerIdentifier;
	protected String appPAN;
	protected String certExpDate;
	protected String certSerialNum;
	protected String hashAlgo;
	protected String pubKeyAlgoIndicator;
	protected String pubKeyLength;
	protected String pubKeyExponentLength;
	protected String pubKey;
	protected String pubKeyRemainder;
	protected String pubKeyExponent;
	protected String staticData2Authenticated;
	
	public static final int ISSUER_CERT_CONST_LENGTH = 36;
	public static final int ICC_CERT_CONST_LENGTH = 42;



	/*
	 * Setter
	 */
	public void setCertificateFormat(String certificateFormat) {
		this.certificateFormat = certificateFormat;
	}


	public void setIssuerIdentifier(String issuerIdentifier) {
		this.issuerIdentifier = issuerIdentifier;
	}


	public void setAppPAN(String appPAN) {
		this.appPAN = appPAN;
	}


	public void setCertExpDate(String certExpDate) {
		this.certExpDate = certExpDate;
	}


	public void setCertSerialNum(String certSerialNum) {
		this.certSerialNum = certSerialNum;
	}


	public void setHashAlgo(String hashAlgo) {
		this.hashAlgo = hashAlgo;
	}


	public void setPubKeyAlgoIndicator(String pubKeyAlgoIndicator) {
		this.pubKeyAlgoIndicator = pubKeyAlgoIndicator;
	}


	public void setPubKeyLength(String pubKeyLength) {
		this.pubKeyLength = pubKeyLength;
	}


	public void setPubKeyExponentLength(String pubKeyExponentLength) {
		this.pubKeyExponentLength = pubKeyExponentLength;
	}


	public void setPubKey(String pubKey) {
		this.pubKey = pubKey;
	}


	public void setPubKeyRemainder(String pubKeyRemainder) {
		this.pubKeyRemainder = pubKeyRemainder;
	}


	public void setPubKeyExponent(String pubKeyExponent) {
		this.pubKeyExponent = pubKeyExponent;
	}


	public void setStaticData2Authenticated(String staticData2Authenticated) {
		this.staticData2Authenticated = staticData2Authenticated;
	}


	public void setCAKeyByteLength(int cAKeyByteLength) {
	}


	public void setIssuerKeyByteLength(int issuerKeyByteLength) {
	}

	//Getter
	public String getPubKeyRemainder() {
		return pubKeyRemainder;
	}

	public String getPubKeyExponent() {
		return pubKeyExponent;
	}
	
	
	//Util
	protected String formatPubKeyforCert(int SignKeyByteLen, String Key2Format, final int CERT_CONST_DATA_LEN) {

		StringBuilder output = new StringBuilder();
		int pubKeyByteLen = Key2Format.length()/2;

		if(pubKeyByteLen <= CERT_CONST_DATA_LEN) {
			//Padding "BB"
			int numByte2Pad = SignKeyByteLen - CERT_CONST_DATA_LEN - pubKeyByteLen;
			output.append(Key2Format);

			for(int i=0; i<numByte2Pad; i++) {
				output.append("BB");
			}

			pubKeyRemainder = null;

		} else {
			//Split pub key
			output.append(Key2Format.substring(0, (SignKeyByteLen - CERT_CONST_DATA_LEN)*2));

			int pubKeyRemainderByteLen = (Key2Format.length() /2) - SignKeyByteLen + CERT_CONST_DATA_LEN;
			pubKeyRemainder = Key2Format.substring(Key2Format.length() - (pubKeyRemainderByteLen*2), Key2Format.length());

		}

		return output.toString();
	}

	
	
	
}
