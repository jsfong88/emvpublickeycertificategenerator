package certificate;

import java.security.NoSuchAlgorithmException;

import crypto.HashGenerator;
import crypto.Rsa;
import crypto.SignedCertificateGenerator;

public class IssuerPubKeyCertificate extends EMVCertificate{

	public String generateIssuerCertHash(int CAKeyByteLength) {

		String output = null;

		StringBuilder str = new StringBuilder();
		str.append(certificateFormat);
		str.append(issuerIdentifier);
		str.append(certExpDate);
		str.append(certSerialNum);
		str.append(hashAlgo);
		str.append(pubKeyAlgoIndicator);
		str.append(pubKeyLength);
		str.append(pubKeyExponentLength);

		//Append public key
		str.append(formatPubKeyforCert(CAKeyByteLength, pubKey, ISSUER_CERT_CONST_LENGTH));
		if(pubKeyRemainder != null) {
			str.append(pubKeyRemainder);
		}		
		str.append(pubKeyExponent);

		System.out.println("Data to hash = " + str);

		try {
			HashGenerator hash = new HashGenerator("SHA-1");
			output = hash.getHash(str.toString());

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}



	public String generateIssuerCert2Sign(int CAKeyByteLength, String hash) {

		StringBuilder str = new StringBuilder();
		str.append("6A");
		str.append(certificateFormat);
		str.append(issuerIdentifier);
		str.append(certExpDate);
		str.append(certSerialNum);
		str.append(hashAlgo);
		str.append(pubKeyAlgoIndicator);
		str.append(pubKeyLength);
		str.append(pubKeyExponentLength);
		str.append(formatPubKeyforCert(CAKeyByteLength, pubKey, ISSUER_CERT_CONST_LENGTH));
		str.append(hash);
		str.append("BC");

		return str.toString();	

	}

	public String generateSignIssuerCert(Rsa CAKey) {

		int CAKeyLen = CAKey.getN().bitLength() /8;
		System.out.println("CAKeyLen" + CAKeyLen);

		String hash = generateIssuerCertHash(CAKeyLen);		
		System.out.println("Hash : " + hash);

		String cert2Sign = generateIssuerCert2Sign(CAKeyLen, hash);
		System.out.println("Cert2Sign: " + cert2Sign);		

		SignedCertificateGenerator certificateGenerator = new SignedCertificateGenerator(CAKey);		
		return certificateGenerator.signCertificate(cert2Sign);

	}


}
