package certificate;

import java.security.NoSuchAlgorithmException;

import crypto.HashGenerator;
import crypto.Rsa;
import crypto.SignedCertificateGenerator;

public class IccPubKeyCertificate extends EMVCertificate{


	public String generateIccCertHash(int issuerKeyByteLength) {

		String output = null;

		StringBuilder str = new StringBuilder();
		str.append(certificateFormat);
		str.append(appPAN);
		str.append(certExpDate);
		str.append(certSerialNum);
		str.append(hashAlgo);
		str.append(pubKeyAlgoIndicator);
		str.append(pubKeyLength);
		str.append(pubKeyExponentLength);
		str.append(formatPubKeyforCert(issuerKeyByteLength, pubKey, ICC_CERT_CONST_LENGTH));
		if(pubKeyRemainder != null) {
			str.append(pubKeyRemainder);
		}
		str.append(pubKeyExponent);

		if(staticData2Authenticated != null) {
			str.append(staticData2Authenticated);
		}

		System.out.println("Data to hash = " + str);

		try {
			HashGenerator hash = new HashGenerator("SHA-1");
			output = hash.getHash(str.toString());

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return output;

	}

	public String generateIccCert2Sign(int issuerKeyByteLength, String hash) {

		StringBuilder str = new StringBuilder();
		str.append("6A");
		str.append(certificateFormat);
		str.append(appPAN);
		str.append(certExpDate);
		str.append(certSerialNum);
		str.append(hashAlgo);
		str.append(pubKeyAlgoIndicator);
		str.append(pubKeyLength);
		str.append(pubKeyExponentLength);
		str.append(formatPubKeyforCert(issuerKeyByteLength, pubKey, ICC_CERT_CONST_LENGTH));
		str.append(hash);
		str.append("BC");

		return str.toString();
	}

	public String generateSignIccCert(Rsa IssuerKey) {

		int IssuerKeyLen = IssuerKey.getN().bitLength() /8;
		System.out.println("IssuerKeyLen" + IssuerKeyLen);
		
		String hash = generateIccCertHash(IssuerKeyLen);		
		System.out.println("hash" + hash);
		
		String cert2Sign = generateIccCert2Sign(IssuerKeyLen, hash);
		System.out.println("cert2Sign" + cert2Sign);
		
		SignedCertificateGenerator certificateGenerator = new SignedCertificateGenerator(IssuerKey);		
		return certificateGenerator.signCertificate(cert2Sign);
	}

}
