package main;

import crypto.Rsa;
import util.DataHelper;

public class App {

	public static void main(String[] args) {
	

	}
	
	public static void printRsaKey(Rsa key, String keyName) {
		System.out.println("Printing Rsa Key: " + keyName);
		System.out.println("modulus: " + DataHelper.bigIntegertoString(key.getN()));
		System.out.println("public exponent: " + DataHelper.bigIntegertoString(key.getE()));
		System.out.println("private exponent: " + DataHelper.bigIntegertoString(key.getD()));
		System.out.println("\n\n");
	}

}
