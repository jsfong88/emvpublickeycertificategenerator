package test;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import certificate.EMVCertificate;
import certificate.IccPubKeyCertificate;
import certificate.IssuerPubKeyCertificate;
import crypto.SignedCertificateGenerator;
import crypto.HashGenerator;
import crypto.Rsa;
import crypto.SigningAlgo;
import util.DataHelper;

class UnitTest {

	@Test
	void test_encrypt() {
		String sMsg = "97AB7F3F0F0FD71D2C7356B79214B510294597F06E1C6E412EF32FECCD9E72EFA9E742F8DBA16E0A6D4C2B173A6BB3CD8B9C214DF8791E740499B18B3CBE7030A523A86D0E3174C325218648EACA772245DABCFE801F09DA8D70610C0C75138445FD9D0B52F1464852AD6A1A2E2DEAD8AA5594AE690EA73F9D4004CBA5C74AD62FAEBF4D74ED46029015B66C741BEB65AA5E182F48C099C6B4B2DD4E04D38EAFA6CBFDA677AE465C3735AC37060A517C";
		String sModulus = "AEAC5DB7F879F5C0DAB6CE358CBD6AA4213D13E7635BE3886CCE9304161D7A9E9FF3C26412957A8318C90602946A297BF5235F7FDD4137B3762D69A25C0889BF839A03C6392CBB325FBC87B892E9C49E8F005F3454D29CD9C545259405E909F040CC2877B6CE9DD1E02D79224C8C3312FB1B223B26414934D0618B7E329C25039C1D98683376637804A2B26F62301EEAD2531D5D3E62BC507EF88FFB15A0101E64A6EA209A72DDA08F4ADAF04E1C52D9";
		String sExpectedOutput ="6A02976173FF1216000002010190018E7B16F633F392F85969318050D57EEB1CC424291CB8D04A991BA970D80AB69B862F7031F2AA443727AD4954317455DAFF6BB530ECAE9E10C7743FA68C984A8ED6675EAB9772AA0A3CB5D78E6CB3FF206E43FC76374060D1F5BF721BE922720AA9161F8BB0ADE390E7B6FF40DFE5A0615AFCB80ED33648EE3882167F6C994CE99570CBDD690E3375C7815164FA840EC22FBD3C4993EEA497FB85C93A28E7D118BC";
		
		SigningAlgo rsa = new Rsa (DataHelper.stringtoBigInteger(sModulus), null, DataHelper.stringtoBigInteger("03"));
		SignedCertificateGenerator certificateGenerator = new SignedCertificateGenerator(rsa);		
		String output = certificateGenerator.recoverCertificateSignature(sMsg);		
		assertEquals(sExpectedOutput, output);
	}
	
	
	@Test
	void test_GetPQFromNDE() {
		
		String sP = "f8cede65a749b1b464bc036cc0195717655e68ace61d762e2381cb9215923fcf0e81e440756935540643478d37520f7f8efdc0546aafeb570b93fdda4cebee4b";
		String sQ = "d20be7ce2db5ee29676526c063fffd197de63d509da45b13d3ef76969504b49ce7a50ad771f4e51cffa95f2fc1cf1bd9c38cecab97f528a2d69b991f92d11c59";

		String sExpectedsN = "cc25449dff2a828466b23ada0faab427ffbe75aeb20c410e02a1e92c5a26598878167d844531f516250e5075bdd6c07d72174ac80abaa76fdeb4ea126a0b3e839fd53e112be7c8fd77b087e68e0b5ebc504cd7024b8ee0b715fa9e6b1c7d29f294850a0c9dd96cfda43c3806c0ba952a6d3f9cbbf9fdb60a40b429e7dd510c13";
		String sExpectedsD = "8818d86954c701ad99cc273c0a71cd6fffd44e7476b2d60957169b72e6c43bb0500efe582e214e0ec35ee04e7e8f2afe4c0f873007271a4a94789c0c46b229abe351a53e39f01b6a725f93d0f14c07079e0576032fde0a4e145b92d6f6997904699411f879a78c5dbedfb6312fbb9be0bc789fd2a4e5c1603f030c9ea90d564b";
		SigningAlgo rsa = new Rsa (DataHelper.stringtoBigInteger(sP), DataHelper.stringtoBigInteger(sQ), BigInteger.valueOf(3), 1);
		String sN = DataHelper.bigIntegertoString(((Rsa) rsa).getN());
		String sD = DataHelper.bigIntegertoString(((Rsa) rsa).getD());
		
		assertTrue(sExpectedsN.equalsIgnoreCase(sN));
		assertTrue(sExpectedsD.equalsIgnoreCase(sD));
		
	}
	
	@Test
	void test_hash() {
		try {			
			String sExpectedHash = "32f86e6ec9f7e7b4c54b8cc543fa78e12ee604df"; 
			String sMsg = "049761739001010010FFFF121600000501018001E0EF4EF84EC2CCA5C9843E2E02F312B768E85C9864694D87D828D498247197384307B5D5CB2ECEC35AFB3B57E4F3A2715A0C1EB8D3D71A3C9EEE93C3DAAF481A6DA0124D7972758D711A5F401A4850EA87E6E68F6BC8B6B180E2D938B9B2FCF6EADBA7E2FA015CCAEA658A31866B7D1C72A0B0B2D7A5BB5BB6ED2D5716E22A9B035A0897617390010100105F340100";
		
			HashGenerator hashGenerator = new HashGenerator("SHA-1");
			assertTrue(sExpectedHash.equalsIgnoreCase(hashGenerator.getHash(sMsg)));
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	void test_iccCert() {
		
		IccPubKeyCertificate iccCert = new IccPubKeyCertificate();
		iccCert.setCertificateFormat("04");
		iccCert.setAppPAN("9761739001010010FFFF");
		iccCert.setCertExpDate("1216");
		iccCert.setCertSerialNum("000005");
		iccCert.setHashAlgo("01");
		iccCert.setPubKeyAlgoIndicator("01");
		iccCert.setPubKeyLength("80");
		iccCert.setPubKeyExponentLength("01");
		iccCert.setPubKey("E0EF4EF84EC2CCA5C9843E2E02F312B768E85C9864694D87D828D498247197384307B5D5CB2ECEC35AFB3B57E4F3A2715A0C1EB8D3D71A3C9EEE93C3DAAF481A6DA0124D7972758D711A5F401A4850EA87E6E68F6BC8B6B180E2D938B9B2FCF6EADBA7E2FA015CCAEA658A31866B7D1C72A0B0B2D7A5BB5BB6ED2D5716E22A9B");
		iccCert.setPubKeyExponent("03");
		
		String hash = iccCert.generateIccCertHash(144);
		
		String sExpectedHash = "0C0F116F785F8CB6D6248204BC25807FE54E61A5";
		assertTrue(sExpectedHash.equalsIgnoreCase(hash));
		
		String sExpectedRemainder = "5CCAEA658A31866B7D1C72A0B0B2D7A5BB5BB6ED2D5716E22A9B";
		assertTrue(sExpectedRemainder.equalsIgnoreCase(iccCert.getPubKeyRemainder()));
		
	}
	
	@Test
	void test_issuerCert() {
		
		IssuerPubKeyCertificate issuerCert = new IssuerPubKeyCertificate();
		issuerCert.setCertificateFormat("02");
		issuerCert.setIssuerIdentifier("976173FF");
		issuerCert.setCertExpDate("1216");
		issuerCert.setCertSerialNum("000002");
		issuerCert.setHashAlgo("01");
		issuerCert.setPubKeyAlgoIndicator("01");
		issuerCert.setPubKeyLength("90");
		issuerCert.setPubKeyExponentLength("01");
		issuerCert.setPubKey("8E7B16F633F392F85969318050D57EEB1CC424291CB8D04A991BA970D80AB69B862F7031F2AA443727AD4954317455DAFF6BB530ECAE9E10C7743FA68C984A8ED6675EAB9772AA0A3CB5D78E6CB3FF206E43FC76374060D1F5BF721BE922720AA9161F8BB0ADE390E7B6FF40DFE5A0615AFCB80ED33648EE3882167F6C994CE99570CBDD690E3375C78151646D871F1B");
		issuerCert.setPubKeyExponent("03");
		
		String hash = issuerCert.generateIssuerCertHash(176);
		
		String sExpectedHash = "FA840EC22FBD3C4993EEA497FB85C93A28E7D118";
		assertTrue(sExpectedHash.equalsIgnoreCase(hash));
		
		String sExpectedRemainder = "6D871F1B";
		assertTrue(sExpectedRemainder.equalsIgnoreCase(issuerCert.getPubKeyRemainder()));
		
	}
	
	@Test
	/*
	 * Test generate Issuer Public key certificate & Icc Public Key certificate 
	 * using CA key, Issuer Key
	 */
	void test_signedCertGenerator() {
		
		//generate CA key - 176
		final int CAKeyBitLen = 1408;
		
		String sN = "80B6171FF1372744ACD4E78301E800AB3CF4C5BEA1A501A1F36FBDD71D9F594BB8A0231E62BA0CCF7596E8C1295951F15F8105C8E9738B4CC45FE3D10B9EB37DFE4741F97564718222D6829E436EA1DB10B81C71B6CA824099894EC1078E3909688B14473C1647D1A0CFBED5004C60406490BE0DA2F3FC404E039F2AEB0990859FFA7D1824DC16571DD1DCBBD2CDA9E483E828D7590F1A48C4757EB3D4B3506031FCFF9E58D7A45BFEA04BFE199843A5";
		String sD = "2F369C34F35EAFE0D9251FE74683BBFB14591550602B0EF6413DF3FD3CB87F20EF908E514F15C02D398D29C3F14EFA67F80873FEC6E3C36D5D7B6F2DF77D378A38041B874E3092129284ED54F0975C77E722220CF468446EB8ACD05C8C8C08EE16EE54C14DDF4B9E40D0228DC82B5CAF834D52769170E47FF83E13206712E822091C8A23DA1F390D305274383C4E0DA11099B257329DF71BC60FEB72CD2FBBA70D2724B15604D0837621D30EDECFB1E1";
		String sE = "010001";
		Rsa CAKey = new Rsa (DataHelper.stringtoBigInteger(sN), DataHelper.stringtoBigInteger(sD),DataHelper.stringtoBigInteger(sE));
		printRsaKey(CAKey, "CA Key");
		
		//generate issuer key - 144
		final int issuerKeyBitLen = 1152;
		String sN2 = "A8CEDC963B7C7AA82807DA07A1D005A9310AEAE9D16B29524FEC61F7791C5FD763BD008BD2AA387459F0D4EEF922DBCFA71DB5FC7A7E488613F16394672CAD07C90683D07089FF2E16A367AA81C578B2BAACE1491BE4341D57A54DA3E28E9675BA69C3AB70041AE8D299D4349F7C38D4E8A6DD094FC959BC84F14B5AFC0559D9D83D2743CCBB8F4BA4B950241D1FA19D";
		String sD2 = "4EFFAFFFA68B14A3C9481362EAF8FD0D36E453176567EF08945A9D6641096458DA5643012C6F78C80AA7481F25FC883DDF8969F76423E130CDF8738FA9D12090BA25105EE5ABCEFD2E10BD5D3F5041CFC9373CE6F092227D0EAC74F2617DC3F30B31C436FB325E64F94A1A480957A9005EC49C6596657B56F87CA08159ACB67266273140A4D887F7EDBF41C5F93771C1";
		String sE2 = "010001";
		Rsa issuerKey = new Rsa (DataHelper.stringtoBigInteger(sN2), DataHelper.stringtoBigInteger(sD2),DataHelper.stringtoBigInteger(sE2));
		printRsaKey(issuerKey, "issuer Key");
		
		//generate icc key - 128'
		final int iccKeyBitLen = 1024;
		String sP3 = "F5432CA766BA8D8F9ED0C05873B14FAC22DF897C884CCB4285AAF9789A01810B6CBDD5D5CC5189CEE3BDB2A33014F7EBEFBE211D40BD7E9F5770D3FC8A3D2589";
		String sQ3 = "E97423628894833FDB045DBF609522B7BDC338D31792ECB18E00466505FFAE29B64E2C73782C18448D294701837139B919E0B4BF42772A7D0449A2489D149DB5";
		String sE3 = "010001";
		Rsa iccKey = new Rsa(DataHelper.stringtoBigInteger(sP3), DataHelper.stringtoBigInteger(sQ3),  DataHelper.stringtoBigInteger(sE3), iccKeyBitLen);
		printRsaKey(iccKey,  "icc Key");
		
		//Prepare Issuer Certificate		
		IssuerPubKeyCertificate issuerCert = new IssuerPubKeyCertificate();
		issuerCert.setCertificateFormat("02");
		issuerCert.setIssuerIdentifier("976173FF");
		issuerCert.setCertExpDate("1225");
		issuerCert.setCertSerialNum("000002");
		issuerCert.setHashAlgo("01");
		issuerCert.setPubKeyAlgoIndicator("01");
		issuerCert.setPubKeyLength(DataHelper.integerToHexaStr(issuerKeyBitLen/8, 2));
		issuerCert.setPubKeyExponentLength("03");
		issuerCert.setPubKey(DataHelper.bigIntegertoString(issuerKey.getN()));
		issuerCert.setPubKeyExponent(DataHelper.bigIntegertoString(issuerKey.getE()));
		String sIssuerCert = issuerCert.generateSignIssuerCert(CAKey);
				
		System.out.println("sIssuerCert: " + issuerKey.getE().bitLength() /8);
		System.out.println("sIssuerCert: " + sIssuerCert);
		System.out.println(issuerCert.getPubKeyRemainder());
		System.out.println(issuerCert.getPubKeyExponent());
		
		//Prepare ICC certificate
		IccPubKeyCertificate iccCert = new IccPubKeyCertificate();
		iccCert.setCertificateFormat("04");
		iccCert.setAppPAN("9761739001FFFFFFFFFF");
		iccCert.setCertExpDate("1225");
		iccCert.setCertSerialNum("000005");
		iccCert.setHashAlgo("01");
		iccCert.setPubKeyAlgoIndicator("01");
		iccCert.setPubKeyLength(DataHelper.integerToHexaStr(iccKeyBitLen/8, 2));
		iccCert.setPubKeyExponentLength("03");
		iccCert.setPubKey(DataHelper.bigIntegertoString(iccKey.getN()));
		iccCert.setPubKeyExponent(DataHelper.bigIntegertoString(iccKey.getE()));
		String sICCCert = iccCert.generateSignIccCert(issuerKey);
		
		System.out.println("sIssuerCert: " + iccKey.getE().bitLength() /8);
		System.out.println("sICCCert: " + sICCCert);
		System.out.println(iccCert.getPubKeyRemainder());
		System.out.println(iccCert.getPubKeyExponent());
		
		
		String sExpectedIssuerCert = "1B3C17A5DF4C2E6492507BF8A792524692D0DC06C57432E4232DF18453DA601DDDF9252EB824AA2F195AFA10503A38AFA26F1C2DA17C18B92BCC61AA6E83442946A98A7A149F202EE603FD003FACA0A0EEB06CE0928C6975746C3E973990C0035C2DE3193F7966FCDF5C638C0862148260117412C005F284B371B4A765012B7317E729945F71A297E6A6130CA4501F7530E7D5AEE0B901E8F4EEAEB61307D0EB35005448C4FBC944B09C9039BA6C9FCC";
		String sExpectedIccCert = "7E67AEE2CDDD07AA431F7D1ABF98817F1572FD0DA747AA06FD98F445038E0E2559BE7D40F149C288FE162779B04B794B672C5ED40EAAFCF3F84F9D3F3E8F75E6AAF7E0C8B4BEE9B65336DED94115B177DE6200FB03C6E272C35250FA9727FDF53309536BEF7169B281D325C0FD85839351EC5ED501FB0026BB5E39C6B638AEF1EA97FA6829A7A54FE8AE72FA5A8C2620";
		Assert.assertEquals(sExpectedIssuerCert, sIssuerCert);
		Assert.assertEquals(sExpectedIccCert, sICCCert);
	}
	
	
	public static void printRsaKey(Rsa key, String keyName) {
		System.out.println("Printing Rsa Key: " + keyName);
		System.out.println("modulus: " + DataHelper.bigIntegertoString(key.getN()));
		System.out.println("public exponent: " + DataHelper.bigIntegertoString(key.getE()));
		System.out.println("private exponent: " + DataHelper.bigIntegertoString(key.getD()));
		System.out.println("\n\n");
	}
}
